# The *hypotenuser* package

This package accompanies the book [Testing R Code](https://www.crcpress.com/Testing-R-Code/Cotton/p/book/9781498763653).  Feel free to click the link and buy a copy.

It contains two simple functions to calculate the hypotenuse, one written in R, and one written in C++.  There are R-level and C++ level tests to accompany the functions.

If you are following along with the book, take a look at the [Commits](https://bitbucket.org/richierocks/hypotenuser/commits) page to see how each command affects the package.

You can also install the package to browse its code.

First install the *devtools* package:

```{r}
install.packages("devtools")
```

Then you can install this package using:

```{r}
devtools::install_bitbucket("richierocks/hypotenuser")
```

# The code used to create this package

## [Step 1](https://bitbucket.org/richierocks/hypotenuser/commits/189fe19c4098c463c34557549cfdedfb37e7e087): call devtools::create

```{r}
library(devtools)
descriptionDetails <- list(
  Title       = "Calculate Hypotenuses",
  Version     = "0.0-1",
  Author      = "Summer Squares [aut,cre]",
  Maintainer  = "Summer Squares <s@sq.com>",
  Description = "A hypotenuse fn, plus tests!",
  License     = "GPL-3",
  URL         = "https://package-homepage.com",
  BugReports  = "https://package-homepage.com/issues"
)
create("hypotenuser", description = descriptionDetails)
```

## [Step 2](https://bitbucket.org/richierocks/hypotenuser/commits/37651f1622c80b4790817964d056274a022b519e): Add the hypotenuse function 

```{r}
writeLines(
  "#' Calculate hypotenuses
  #'
  #' Calculates the hypotenuse, using the obvious algorithm.
  #' @param x A numeric vector.
  #' @param y Another numeric vector.
  #' @return The hypotenuse.
  #' @export
  hypotenuse <- function(x, y)
  {
  sqrt(x ^ 2 + y ^ 2)
  }",
  con = "hypotenuser/R/hypotenuse.R"
)
```

## [Step 3](https://bitbucket.org/richierocks/hypotenuser/commits/9ec17555e3d5442b7b15b8baa5cf725c85773047): Add testing infrastructure

```{r}
use_testthat("hypotenuser")
```

## [Step 4](https://bitbucket.org/richierocks/hypotenuser/commits/9d8112487be646bab91afa1c84dd4bf08d831202): Add tests

```{r}
writeLines(
  "context('Testing the hypotenuse function')
test_that(
  'hypotenuse, with inputs 5 and 12, returns 13',
  {
    expected <- 13
    actual <- hypotenuse(5, 12)
    expect_equal(actual, expected)
  }
)
test_that(
  'hypotenuse, with inputs both 1e300, returns sqrt(2) * 1e300',
  {
    expected <- sqrt(2) * 1e300
    actual <- hypotenuse(1e300, 1e300)
    expect_equal(actual, expected)
  }
)",    # etc.
  "hypotenuser/tests/testthat/test-hypotenuse.R"
)
```

## [Step 5](https://bitbucket.org/richierocks/hypotenuser/commits/e2ed6dcc8716b1b86b3b659b31ce86c4765784b8): Run roxygenize

```{r}
library(roxygen2)
roxygenize()
```

## [Step 6](https://bitbucket.org/richierocks/hypotenuser/commits/fa9d098a6d870bbaacf414a3b1ca0f7441200737): Add Rcpp infrastructure

```{r}
use_rcpp()
```

## [Step 7](https://bitbucket.org/richierocks/hypotenuser/commits/b21264f281009ccb6ad437ebd02595411c2de089): Add rcpp-setup.R

```{r}
writeLines(
  "#' @useDynLib yourpkg
#' @importFrom Rcpp sourceCpp
NULL",
  "R/rcpp-setup.R"
)
```

## [Step 8](https://bitbucket.org/richierocks/hypotenuser/commits/2a54132ec3ad72e9f969ee896966721e34886fae): Re-run roxygenize

```{r}
roxygenize()
```

## [Step 9](https://bitbucket.org/richierocks/hypotenuser/commits/38a722ec4e8eceb0ff1f9abf995f8b3977607946): Add Catch infrastructure

```{r}
library(testthat)
use_catch()
```

## [Step 10](https://bitbucket.org/richierocks/hypotenuser/commits/66048afd1bf20d8886001813173ff9127adfa7d2): Update description to include catch info

```{r}
devtools:::add_desc_package(
  ".", "LinkingTo", "testthat"
)
```
## [Step 11](https://bitbucket.org/richierocks/hypotenuser/commits/77e23d2161fdd38df8a4220f148e4e3607913a3c): Add C++ hypotenuse function

```{r}
writeLines(
  "#include <Rcpp.h>
#include <testthat.h>
#include <math.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericVector hypotenuseCpp(NumericVector x, NumericVector y) {
  return sqrt(x * x + y * y);
}",
  "src/hypotenuse.cpp"
)
```

## [Step 12](https://bitbucket.org/richierocks/hypotenuser/commits/c5b312dc9fe5efd7f7bdae3520d06fef5d52b7e6): Add R-level documentation for the C++ function

```{r}
writeLines(
  "#' Calculate hypotenuses
#'
#' Calculates the hypotenuse, using C++.
#' @param x A numeric vector.
#' @param y Another numeric vector.
#' @return The hypotenuse.
#' @name hypotenuseCpp
#' @export
NULL",
  "R/hypotenuseCpp.R"
)
```

## [Step 13](https://bitbucket.org/richierocks/hypotenuser/commits/21efd79606641fb97c1823c0be78c474ae9464bc): Add an R-level test for the C++ function

```{r}
writeLines(
  'test_that(
  "hypotenuseCpp works at the R level",
  {
    x <- c(3, 5, 8)
    y <- c(4, 12, 15)
    expected <- c(5, 13, 17)
    actual <- hypotenuseCpp(x, y)
    expect_equal(actual, expected)
  }
)',
  "tests/testthat/test-hypotenuserCpp.R"
)
```

## [Step 14](https://bitbucket.org/richierocks/hypotenuser/commits/463f9fc4015e655db1681725f08c26c52ade7f72): Add a C++-level test for the C++ function

```{r}
cat(
  '

context("test hypotenuseCpp") {
test_that("hypotenuseCpp works at the C++ level") {
  NumericVector x = NumericVector::create(3.0, 5.0, 8.0);
  NumericVector y = NumericVector::create(4.0, 12.0, 15.0);
  NumericVector expected = NumericVector::create(5.0, 13.0, 17.0);
  NumericVector actual = hypotenuseCpp(x, y);
  expect_true(is_true(all(actual == expected)));
}
}',
  file = "src/hypotenuse.cpp",
  sep = "\n",
  append = TRUE
)
```

## [Step 15](https://bitbucket.org/richierocks/hypotenuser/commits/a7017b7d70c656d4d7f049ddf27e6dc3f5cbd0cf): Re-run roxy

```{r}
roxygenize()
```

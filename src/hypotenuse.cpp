#include <Rcpp.h>
#include <testthat.h>
#include <math.h>
using namespace Rcpp;
// [[Rcpp::export]]
NumericVector hypotenuseCpp(NumericVector x, NumericVector y) {
  return sqrt(x * x + y * y);
}

context("test hypotenuseCpp") {
  test_that("hypotenuseCpp works at the C++ level") {
    NumericVector x = NumericVector::create(3.0, 5.0, 8.0);
    NumericVector y = NumericVector::create(4.0, 12.0, 15.0);
    NumericVector expected = NumericVector::create(5.0, 13.0, 17.0);
    NumericVector actual = hypotenuseCpp(x, y);
    expect_true(is_true(all(actual == expected)));
  }
}
